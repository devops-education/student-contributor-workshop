# GitLab Student Contributor Workshop

This repository contains the [GitLab Student Contributor Workshop](https://devops-education.gitlab.io/student-contributor-workshop) created by the GitLab for Education team.

This workshop can be conducted in person or completed asynchronously.

Coming soon: We'll open up the workshop for contributions as well.
