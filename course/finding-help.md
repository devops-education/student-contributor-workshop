---
title: Finding help
nav_order: 5
parent: GitLab Student Contributor Workshop
---

# How to find help while contributing

If you need any help while contributing to GitLab, below are some of the resources that are available.

1. Ask questions on the [Contributors Gitter Channel](https://gitter.im/gitlabhq/contributors). Be sure to introduce yourself as a new student contributor.
2. f you need help with a Merge Request or need help finding a reviewer - get in touch with a [Merge Request Coach](https://about.gitlab.com/job-families/expert/merge-request-coach/). You can ask for help by typing @gitlab-bot help in a comment. Alternatively you can find reviewers & maintainers of Gitlab projects in our [handbook](https://about.gitlab.com/handbook/engineering/projects/) and [mention](https://docs.gitlab.com/ee/user/group/subgroups/#mentioning-subgroups) them in a comment.
3. Reviewers/maintainers of GitLab projects are also [listed here](https://about.gitlab.com/handbook/engineering/projects/#gitlab-ce).
4. For any other questions/feedback, you can send an email to `contributors@gitlab.com`.

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/ready-set-contribute/)
