---
title: Create a GitLab.com Account
nav_order: 3
parent: GitLab Student Contributor Workshop
---

# Sign up for a GitLab.com account
In order to complete the workshop, you'll need to have a GitLab.com account. If you already have an account on GitLab.com and wish to use your existing account, you do not need to complete this step.

# Validation
Please note, GitLab requires validation using a credit or debit card in order to use certain features, including CI/CD pipelines. We do not charge the card and we do not keep it on file to charge later. It is being used as a way to prevent [crypto mining abuse](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).

# Steps
1. Visit [https://gitlab.com](https://gitlab.com)
2. Click on Register now or Sign in with an existing account.
3. Log into your account.

# Update your profile
Update your GitLab profile! Please consider including that you are a student and indicate your University or current affiliation (code academies, bootcamps, .etc) in your GitLab Profile Bio. This will help engineers see that you are a student! Include a picture or any other details you'd like.

Feel free to add more information to your profile page with a README file. Learn more [here](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme).

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/getting-started/)
