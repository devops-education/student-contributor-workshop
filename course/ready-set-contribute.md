---
title: Ready, set, contribute
nav_order: 6
parent: GitLab Student Contributor Workshop
---


# Ready Set Contribute!

Once you've completed the getting started steps of creating and account and finding your contribution, you are ready to get started!.

The steps are slightly different depending on whether you are choosing to fix a typo in our handbook or work on a Documentation issue. We'll cover both sets of instructions here or point you in the right direction so you get underway.

## Fixing a typo in the Handbook.

Once you found your typo, follow the steps to make a contribution.

1. Navigate to the page in the handbook that has the typo you wish to fix from this [list of typos we prepared](https://gitlab.com/-/snippets/2259819). Note:  You can also find your own typo. If you do, skip to the next numbered step.
  * Copy the URL starting directly after `sites/handbook/source/handbook/`and up to the last / before `index.html`. For example, line 9 has `sites/handbook/source/handbook/business-technology/data-team/data-catalog/available_to_renew/index.html.md:31: futher ==> further` and you want to copy `business-technology/data-team/data-catalog/available_to_renew/`.
  * Open up a tab of your browser and go to `https://about.gitlab.com/handbook/` then past the URL you copied onto the end to open up the page with the typo. It should look like this for our example `https://about.gitlab.com/handbook/business-technology/data-team/data-catalog/available_to_renew/`. This will open the page of the handbook.
  * Find the typo using CTRL F.
1. Scroll down to the bottom of the page and click on `Edit in WebIDE`. The GitLab WebIDE will open.
1. Next `Fork` the gitlab-com project by clicking the `Fork project` button on the upper left. Because this is a large project, it may take a few minutes to fork. A forking in progress notice will appear on the page.  
  * A [`Fork`](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) is a personal copy of the repository and all its branches which you create in namespace of your choice.

![Fork.png]({{site.baseurl}}/attached_files/images/Fork.png)
![Fork_progress.png]({{site.baseurl}}/attached_files/images/Fork_progress.png)

1. After forking is complete, the WebIDE will default to the root of the repository. Use the file tree to navigate back to the handbook page with the typo. In this example, click through the folders to find `sites/handbook/source/handbook/business-technology/data-team/data-catalog/available_to_renew`.
1. Find the typo and make the fix.
1. Click `Commit` in the left-hand pane.
1. The WebIDE will then show the differences (before commit and after) on the right hand side and on the left an option to add a commit message, branch options, and a check box for creating a new merge request. In this example, we'll create a new branch and start a new merge request:
  * Type a short descriptive commit message.
  * `Create a new branch`. Name the branch with your last name hyphen and typo.
  * Make sure `Start a new merge request` is  checked.
1. Click `Commit`.
![Commit.png]({{site.baseurl}}/attached_files/images/Commit.png)

1. A `New merge request` creation page will open.
1. Name your merge request and add some basic details. Accept the defaults for `Merge options` and `contributions`. Click `Create new merge request`.
1. After the new merge request is open, review the `Author Checklist`. If you followed the directions, you can check the first two boxes.
  * Note: The CI/CD pipeline will automatically run.
1. Note that the `GitLab Bot` has automatically labeled this MR with `Community contribution`. By using this label, the merge request will appear on the GitLab Commnity Contributions board. If you do not hear back from our team in a few days, please mention `@gitlab-com-community` in the MR for review.
![Commit.png]({{site.baseurl}}/attached_files/images/community-contribution.png)

1. To check on the status of your MR later after, click on `Merge Requests` shortcut from the top navigation bar in GitLab and then search for Author = `yourname`.
  * Note: GitLab team members may comment on the MR and suggest other changes or provide feedback. If a team member @mentions you, a notification will appear in your [TODO list](https://docs.gitlab.com/ee/user/todos.html#access-the-to-do-list) on the upper right of the top bar.
1. A `merged` banner will appear at the top of the MR once it is merged.


## Contributing to Documentation
Step-by-step directions for contributing to Documentation are included in our Documentation [here](https://docs.gitlab.com/ee/development/documentation/workflow.html).

A couple important things to note:
1. Once you find an issue you'd like to work on, in the issue, comment that you want to work on it, mention `@gl-docsteam` and they will assign it to you.
2. Free free to introduce yourself to the participants or issue author in the issue and ask for any additional details.




* Note that if you open the WebIDE directly from the handbook page, it will open the directory containing the `index.html` file. If you need to navigate directly to the index.html of any handbook page, you can do that using the file tree as well. In this example you would navigate to `sites/handbook/source/handbook/business-technology/data-team/data-catalog/available_to_renew` from `gitlab-com/www-gitlab-com/`.
