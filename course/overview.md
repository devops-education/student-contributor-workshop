---
title: Overview of Open Source
nav_order: 1
parent: GitLab Student Contributor Workshop
---

# Overview

## What is open source?

- Software where anyone is freely licensed to use, copy, study, and change the software in any way.
- The source code is openly shared and people are encouraged to voluntarily improve the design of the software .
- Changes made to the code might be kept by the maintainers, people who are in charge of the codebase and what is and isn't merged.
- Code is kept in repositories online.
- Discussion takes place between contributors, maintainers, owners, and the users of the code as well.   

## Why contribute to open source?

- Gain hands-on technical experience.
- Ability to set your own schedule for making contributions.
- Build your technical portfolio with a public record of your contributions.
- Build/expand your network with other open source contributors.  
- Experience in one open source community can be translated to other communities and even to non-open source environments (e.g. companies that are doing “inner-source”).

## What to look for in open source projects?
- Availability of onboarding help.
- Issues for new/first-time contributors.
- Communication channels/forums to communicate with other community members.
- Responsiveness of reviewers/other community members.
- A community code of conduct.

## Open source Projects on GitLab

There are 100s of open source projects hosted on GitLab. [Here is a list](https://about.gitlab.com/solutions/open-source/projects/) of just some of the projects. This is a great list to look through if you are interested in contributing to an open source project.

GitLab supports open source projects through our [GitLab for Open Source Program](https://about.gitlab.com/solutions/open-source/join/)and GitLab Open Source Partners Program.


[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/categories/)
