---
title: Contribution Categories
nav_order: 2
parent: GitLab Student Contributor Workshop
---
## Contribution Categories

There are several diferent areas or categories where contributors can contribute to GitLab.

- [Development](https://about.gitlab.com/community/contribute/development/)
- [Documentation](https://about.gitlab.com/community/contribute/documentation/)
- [Translation](https://about.gitlab.com/community/contribute/translation/)
- [UX Design](https://about.gitlab.com/community/contribute/ux-design/)
- [Community Engagement](https://about.gitlab.com/community/contribute/engagement/)
- [Project templates](https://about.gitlab.com/community/contribute/project-templates/)


Each category has a page dedicate to getting contributors up and running. In general, contributing to Development and and UX Design requires more in-depth software development skills. For example, the core of GitLab itself is written in Ruby on Rails. In order to contribute, developers will need to be able to properly set up a [development environment](https://about.gitlab.com/community/contribute/development/#development).

If you are a beginner, we recommend starting with contributing to GitLab's documentation. GitLab's documentation is written in [markdown files](https://docs.gitlab.com/ee/user/markdown.html), a text file format, that is very easy to edit.

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/create/)
