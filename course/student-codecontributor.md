---
title: GitLab Student Contributor Workshop
nav_order: 1
has_children: true
---

# Workshop outline
1. Overview:
  - Introduction to open source
  - Benefits of contributing to open source
  - What to look for in open source projects
  - Open source projects hosted on GitLab
2. Contribution Categories: Documentation, Translation, Development, User Experience, Templates, and Community
3. Create a GitLab.com account
5. Getting starting - find your spot!
6. How to find help
7. Ready, set, contribute

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/overview/)
