---
title: Getting Started
nav_order: 4
parent: GitLab Student Contributor Workshop
---

# Find a spot to contribute!

This workshop focuses on contributing to GitLab's **handbook** or **documentation** since it is a beginner workshop. This page covers three options for identifying where to contribute:

- Documentation issues
- Handbook typos
- Running a linter to find errors

## Documentation Issues

- From `GitLab.org -- GitLab -- Issues` you can search by label to look for issues that are `good for new contributors` and `accepting merge requests` and `documentation`. [Here is a link](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=created_date&state=opened&label_name[]=good+for+new+contributors&label_name[]=Accepting+merge+requests&label_name[]=documentation) directly to this search.

For example
![goodfornew.png]({{site.baseurl}}/attached_files/images/goodfornew.png)

1. Browse through the issues and find a topic that interests you. The other labels can help you sort and identify topics. For example, the label `feature` indicates the documentation is about a feature while the label `group:configure` indicates that issue is about the configure stage.

Here is a good a example of an issue that suggests adding some missing details to the Debian documentation.

![debian.png]({{site.baseurl}}/attached_files/images/debian.png)

## Handbook typos
If you aren't quite ready to dig into documentation, you are welcome to get started by contributing to our GitLab handbook as well.

Our handbook consists of over 2,000 web pages of text and is updated many times a day.

[Here is a list of typos](https://gitlab.com/-/snippets/2259819) that can be found in our handbook. These typos are generally small changes so we don't have issues open for them.

## Run a linter and find errors.

The [Vale linting tool](https://docs.gitlab.com/ee/development/documentation/testing.html#vale) is is a grammar, style, and word usage linter for the English language. A linter is a tool that analyzes source code.

You can run this tool to find a list of errors and then open a MR to make a contribution.

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/finding-help/)
