---
title: Home
nav_order: 1
description: Workshop on how to contribute to GitLab for students.
permalink: /
---

# GitLab Student Contributor Workshop

Welcome to the GitLab Student Contributor Workshop. This workshop will introduce participants to the concepts of open source, the benefits of contributing to open source, and how to get started contributing. This workshop is tailored for **new contributors** and will focus on contributing to [GitLab's documentation](https://docs.gitlab.com/ee/development/documentation/) and [GitLab's handbook](https://about.gitlab.com/handbook/). The contributions will involve modifications, corrections, or additions to markdown text files. Examples include, correcting errors or typos in our documentation and handbook, expanding on sections, or finding and replacing key terms for updates.

Our goal with this workshop is to highlight the process of contributing and collaborating with an open source project, rather than making significant contributors to a software project itself.

We consider this workshop the first step in the journey of becoming a contributor to open source. We hope that as students learn the process, build community, and enjoy the experience, as they further develop skills they will be interested in contributing to the open source software itself.

![git-community-logo]({{site.baseurl}}/attached_files/images/logo_community.png)

[Next Page](https://devops-education.gitlab.io/student-contributor-workshop/course/student-codecontributor/)
